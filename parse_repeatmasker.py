#!/usr/bin/python

import os
import sets

def main():
    os.system('cut -b25-38,60-98 RM2_out44_1268158347.out > RM2_out44_1268158347.out.txt')
    f = open('RM2_out44_1268158347.out.txt')
    lines = f.readlines()
    f.close()

    pos = {}
    neg = {}
    for line in lines[3:]:
        s = line.split()
        num = int(s[0].split('_')[1])
        print num, s
        name = s[1]
        type = s[2]

        if name not in pos:
            pos[name] = 0 
        if name not in neg:
            neg[name] = 0 

        if num % 2 == 0:
            print num
            pos[name] = pos[name] + 1
        else:
            neg[name] = neg[name] + 1

    names = set()
    names = names.union(pos.keys())
    names = names.union(neg.keys())
    for name in names:
        if (neg[name] < 2 or pos[name] < 5 ):
            continue
        r = float(pos[name]) / float(neg[name])
        if r > 1.2:# or r < 0.7:
            print name, neg[name], pos[name]

    print 'sum negs', sum(neg.values())
    print 'sum pos', sum(pos.values())

    
if __name__=='__main__':
    main()
