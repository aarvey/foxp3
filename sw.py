import sys


## taken from http://www.codesofmylife.com/2011/05/13/smith-waterman-algorithm-for-local-alignment-in-python/


'''Read the similarity matrix from BLOWSUM and return the similarity map of all the combination of the bases'''
def readBLOSUM(fname):
    '''The first row of blowsum matrix'''
    t = ['A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K', 'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V', 'B', 'X', '*']
    ##similarityMatrix = np.loadtxt(fname, delimiter=' ')
    f = open(fname)
    similarityMatrix = [l.strip().split() for l in f.readlines()]
    f.close()
    similarityMatrixMap = dict()
    for i in range(len(t)):
        base1 = t[i]
        for j in range(len(t)):
            base2 = t[j]
            similarityMatrixMap[base1 + base2] = int(similarityMatrix[i][j])
    return similarityMatrixMap

'''Read the similarity matrix from BLOWSUM and return the similarity map of all the combination of the bases'''
def readDNAMAT(fname):
    '''The first row of blowsum matrix'''
    t = ['A', 'C', 'G', 'T', 'N']
    f = open(fname)
    similarityMatrix = [l.strip().split() for l in f.readlines()]
    f.close()
    similarityMatrixMap = dict()
    for i in range(len(t)):
        base1 = t[i]
        for j in range(len(t)):
            base2 = t[j]
            similarityMatrixMap[base1 + base2] = int(similarityMatrix[i][j])
    return similarityMatrixMap


def computeFMatrix(seq1, seq2, gap, similarityMatrixMap, returnFormated=False):
    '''This function creates alignment score matrix
    seq1 : reference sequence
    seq2 : other sequence
    gap : gap penalty '''
    
    rows = len(seq1) + 1
    cols = len(seq2) + 1
    fMatrix = [[0 for i in range(cols)]  for j in range(rows)]
    pointers =  [[0 for i in range(cols)]  for j in range(rows)]

    maxScore = 0
    iOfMax = 0
    jOfMax = 0
            
    for i in range(1, rows):
        for j in range(1, cols):
            mtch = fMatrix[i - 1][j - 1] + similarityMatrixMap[seq1[i - 1] + seq2[j - 1]]
            fMatrix[i][j] = max(0, mtch)
            if fMatrix[i][j] > maxScore :
                iOfMax = i
                jOfMax = j
                maxScore = fMatrix[i][j]
            if not returnFormated:
                continue

            
            delete = fMatrix[i - 1][j] + gap
            insert = fMatrix[i][j - 1] + gap
            fMatrix[i][j] = max(0, mtch, delete, insert)
            
            if(fMatrix[i][j] == 0):
                pointers[i][j] = -1
            elif(fMatrix[i][j] == delete):
                pointers[i][j] = 1
            elif(fMatrix[i][j] == insert):
                pointers[i][j] = 2
            elif(fMatrix[i][j] == mtch):
                pointers[i][j] = 3
                                
            if fMatrix[i][j] > maxScore :
                iOfMax = i
                jOfMax = j
                maxScore = fMatrix[i][j]
               


    if (returnFormated):
        (aligned1, aligned2, startOfAlign1, startOfAlign2) = trackBack(pointers, seq1, seq2, gap, similarityMatrixMap, iOfMax, jOfMax)    
        return formatTrackBack(aligned1, aligned2, startOfAlign1, startOfAlign2, pointers, seq1, seq2, gap, similarityMatrixMap, iOfMax, jOfMax)    

    return maxScore

def formatTrackBack(aligned1, aligned2, startOfAlign1, startOfAlign2, pointers, seq1, seq2, gap, similarityMap, i, j):
    '''Some formatting for displaying alignment'''
    numOfSpacesToAdd1 = {True: 0, False: startOfAlign2 - startOfAlign1}[startOfAlign1 >= startOfAlign2]
    numOfSpacesToAdd2 = {True: 0, False: startOfAlign1 - startOfAlign2}[startOfAlign2 >= startOfAlign1]
    aligned1 = ' ' * numOfSpacesToAdd1 + seq1[:startOfAlign1] + aligned1 + seq1[i:]
    aligned2 = ' ' * numOfSpacesToAdd2 + seq2[:startOfAlign2] + aligned2 + seq2[j:]
    alignPointer = ''
    for cSeq1,cSeq2 in zip(aligned1, aligned2):
        alignPointer += {True: ':', False: ' '}[cSeq1 == cSeq2]

    return (aligned1, aligned2, alignPointer)

    
def trackBack(pointers, seq1, seq2, gap, similarityMap, i, j):
    '''Tracks back to create the aligned sequence pair'''
    alignedSeq1 = ''
    alignedSeq2 = ''
    
    while pointers[i][j] != -1 and i > 0 and j > 0:
        ##print pointers[i][j], i, j
        if pointers[i][j] == 1:
            alignedSeq1 = seq1[i - 1] + alignedSeq1
            alignedSeq2 = '-' + alignedSeq2
            i = i - 1
        elif pointers[i][j] == 2:
            alignedSeq1 = '-' + alignedSeq1
            alignedSeq2 = seq2[j - 1] + alignedSeq2
            j = j - 1
        elif pointers[i][j] == 3:
            alignedSeq1 = seq1[i - 1] + alignedSeq1
            alignedSeq2 = seq2[j - 1] + alignedSeq2
            i = i - 1
            j = j - 1
            
    return (alignedSeq1, alignedSeq2, i, j)
        
if __name__ == "__main__":
    seq1 = 'HEAGAWGHEE'
    seq2 = 'PAWHEAE'
    
    similarityMatrixMap = readBLOSUM("blosum62.txt")
    print computeFMatrix(seq1, seq2, -1000, similarityMatrixMap, False)
    sys.exit(0)
    (alignedSeq1, alignedSeq2, alignPointer) = computeFMatrix(seq1, seq2, -6, similarityMatrixMap, True)
    print alignedSeq1
    print alignPointer
    print alignedSeq2
            
