
import string, os, sys, gzip


class TCR:

    def __init__(self, name, s1, s2):
        self.n = name
        self.s1 = s1
        self.s2 = s2
        self.dna = ''
        self.aa = ''
        self.align = ''
        self.note = ''
        self.barcode = ''
        self.rep = 0

    def tolist(self):
        return [self.n, str(self.rep), self.barcode, self.s1, self.s2, self.dna, self.aa, self.align, self.note]
         
    def __str__(self):
        return '\t'.join(self.tolist())


def df2contig(d, flip=False):
    en = 20
    s1 = d.s1
    s2 = d.s2
    if flip: ts1 = s2; s2=s1; s1=ts1;
    d.barcode = barcode = s2[:9]
    s2e = s2[-en:]
    q = revcomp(s2e)
    idx = s1.find(q)
    if idx < 0: d.note = 'no contig';  return s1, s2, -1
    return s1, s2, idx


def usage():
    print 'hello'

def main():
    fname1 = sys.argv[1];   fname2 = sys.argv[2]; verbose = False
    f = gzip.open(fname1);  lines = f.readlines(9999999);  f.close()
    f = gzip.open(fname2);  lines2 = f.readlines(9999999); f.close();
    
    f = gzip.open(fname1);  lines = f.readlines();  f.close()
    f = gzip.open(fname2);  lines2 = f.readlines(); f.close();

    print 'Total R1 lines:', len(lines)
    print 'Total R2 lines:', len(lines2)
    df = [ TCR(lines[i].strip(), lines[i+1].strip(), lines2[i+1].strip()) for i in range(0,min(len(lines),len(lines2))-4,4)]
    del lines
    del lines2
    print 'Total paired reads:', len(df)
    good_seq = "CTGGTACACAGCAGGTTCTGGGTTCTGGATGT"
    df = [d for d in df if d.s2.find(good_seq) > 0]
    print 'Reads with seq "%s":' % (good_seq,), len(df)

    tcrs = []
    for i in range(len(df)):
        if i % 10000 == 0:  sys.stderr.write('%d\n' % (i))
        ##if i != 5: continue
        ##print i, 'running'
        ##print df[i]

        s1, s2, idx = df2contig(df[i])
        if idx < 0: s1, s2, idx = df2contig(df[i], True)
        
        nchar_shift = len(s1)-len(s2)
        shift_align = '%s%s' % ('-'*(idx), revcomp(s2))
        contig_align = '%s\n%s' % (s1, shift_align)
        df[i].align = shift_align
        if idx < 0: df[i].align = 'NA'
        
        if verbose: print contig_align
        template = '%s%s' % (s1,  revcomp(s2)[(nchar_shift-idx):])
        #cat(sprintf("%s\n", template))
        template = template.replace('N', 'A')
        if verbose: print template
        j = 0   
        while True:
            aa = translate(template)
            cidx = aa.find("IQNPEPAVYQ")
            if cidx > -1: break
            template = template[1:]
            j += 1
            if j > 5: df[i].note="bad frame"; break;
        if cidx > -1 and aa[:cidx+3].find("*") >= 0: df[i].note='stop codon'
        df[i].dna = template
        df[i].aa = aa
      
    from operator import itemgetter, attrgetter

    print 'Total number of raw output TCRs:', len(df)
    #df.sort(lambda d: d.s1, d.barcode)
    rep_count = 0
    df = sorted(df, key=attrgetter("s1", "barcode")) #lambda d: d.s1, d.barcode)
    for i in range(1,len(df)):
        if df[i].barcode==df[i-1].barcode and df[i].s1.find(df[i-1].s1[5:25]) > -1:
            df[i].rep = 1
            rep_count += 1
            ##print i, 'repeat'

    print 'Total PCR reps:', rep_count

    #print df
    ##df = [d  for d in df  if d.note=='']
    f = open('%s.tcrs.raw.xls' % (fname1,), 'w')
    f.write('\n'.join(['\t'.join(d.tolist()) for d in df]))
    f.write('\n')
    f.close()


    tcr_counts = {}
    total_tcrs = 0
    for i in range(1, len(df)):
        if df[i].rep > 0:    continue
        aa = df[i].aa
        idx_idx = aa.find('IQNPEP')
        aa = aa[:idx_idx]
        if idx_idx > 0 and df[i].align!='NA':
            total_tcrs += 1
        if tcr_counts.has_key(aa):
            tcr_counts[aa] += 1
        else:
            tcr_counts[aa] = 1

    print 'Total non PCR TCRs, with IQNPEP, with nice alignment:', total_tcrs

    f = open('%s.tcrs.counts.xls' % (fname1,), 'w')
    f.write('\n'.join(['%d\t%s' % (n,aa) for aa,n in tcr_counts.items()]))
    f.write('\n')
    f.close()


    #oidx = order([d.s2 for d in df], [d.barcode for d in df])
    #print oidx





    
revcomp_table = string.maketrans('ACBDGHKMNSRUTWVYacbdghkmnsrutwvy',
                                 'TGVHCDMKNSYAAWBRTGVHCDMKNSYAAWBR')
def revcomp(seq):
    s = (''.join(seq)).translate(revcomp_table, '\n')[::-1]
    return s

def str2mm(s):
    s = s.split()
    ret = matrix(s, nrow=length(s), ncol=length(s))
    ret[seq(1, length(ret), ncol(ret)+1)] = "."
    ret = apply(ret, 2, paste, collapse="")
    ret = paste(ret, collapse="|")
    return(ret)

dna2aa = {
    'GGG':'Gly','GGA':'Gly','GGT':'Gly','GGC':'Gly','GAG':'Glu','GAA':'Glu',
    'GAT':'Asp','GAC':'Asp','GTG':'Val','GTA':'Val','GTT':'Val','GTC':'Val',
    'GCG':'Ala','GCA':'Ala','GCT':'Ala','GCC':'Ala','AGG':'Arg','AGA':'Arg',
    'AGT':'Ser','AGC':'Ser','AAG':'Lys','AAA':'Lys','AAT':'Asn','AAC':'Asn',
    'ATG':'Met','ATA':'Ile','ATT':'Ile','ATC':'Ile','ACG':'Thr','ACA':'Thr','ACT':'Thr','ACC':'Thr',
    'TGG':'Trp','TGA':'End','TGT':'Cys','TGC':'Cys','TAG':'End','TAA':'End',
    'TAT':'Tyr','TAC':'Tyr','TTG':'Leu','TTA':'Leu','TTT':'Phe','TTC':'Phe',
    'TCG':'Ser','TCA':'Ser','TCT':'Ser','TCC':'Ser',
    'CGG':'Arg','CGA':'Arg','CGT':'Arg','CGC':'Arg',
    'CAG':'Gln','CAA':'Gln','CAT':'His','CAC':'His',
    'CTG':'Leu','CTA':'Leu','CTT':'Leu','CTC':'Leu',
    'CCG':'Pro','CCA':'Pro','CCT':'Pro','CCC':'Pro'}

aa2letter = {
    'Ala':'A','Arg':'R','Asn':'N',
    'Asp':'D','Cys':'C','Gln':'Q',
    'Glu':'E','Gly':'G','His':'H',
    'Ile':'I','Leu':'L','Lys':'K',
    'Met':'M','Phe':'F','Pro':'P',
    'Ser':'S','Thr':'T','Trp':'W',
    'Tyr':'Y','Val':'V','End':'*'}


def translate(s):
    codons = [s[i:(i+3)] for i in range(0,len(s)/3*3,3)]
    r = ''.join([aa2letter[dna2aa[c]] for c in codons])
    return r


if __name__=="__main__":
    main()



