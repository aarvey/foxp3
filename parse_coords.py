#!/usr/bin/python

fname = 'SOLID_0041_FoxP3_F3.unique.csfasta.ma.50.4'
f = open(fname)
lines = f.readlines()
f.close()

print 'Finished reading'

#>559_28_55_F3,4_-9544714.3
#T131312112103121.31031.1312003321110220212310210000
newlines = []
for i,line in enumerate(lines):
    if i%100000==0:
        print i
    if line[0]=='>':
        coord = line.split(',')[1]
        s = coord.split('_')
        chr = s[0]
        s = s[1].split('.')
        loc = int(s[0])
        strand = '+' if loc > 0 else  '-'
        mm = s[1]
        loc = abs(loc)
        s = lines[i+1].strip()
        # Output tag align format
        # chr1    35278165        35278213        TGGGTGGATCACCTGCAGTCAGTAGTTCACGGCCGGCCTGGCCCACA 1000    +
        if strand == '+':
            start = loc
            end = start + len(s) - 1
        else:
            end = loc
            start = end - len(s) + 1
        #mm.strip()
        newlines.append(' \t'.join(['chr'+chr, str(start), str(end), 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', '1000', strand+'\n']))


print 'Finished newlining'

for line in newlines[:100]:
    print line[:-1]



f = open(fname + '.tagAlign', 'w')
f.writelines(newlines)
f.close()
