source("~/unagidata/foxp3/seq_globals.R")
source("~/unagidata/foxp3/seq_peaks.R")
source("~/unagidata/foxp3/seq_expr.R")
source("~/unagidata/foxp3/seq_act.R")
source("~/unagidata/foxp3/seq_cofactor.R")
source("~/unagidata/foxp3/seq_fig1.R")
source("~/unagidata/foxp3/seq_footprints.R")
source("~/unagidata/foxp3/seq_motif.R")
source("~/unagidata/foxp3/seq_old.R")
source("~/unagidata/foxp3/seq_qual.R")
source("~/unagidata/foxp3/gse_sets.R")
source("~/ll-utils/chromatin/itr.R")
source("~/ll-utils/mirna/ks.R")
source("~/ll-utils/mirna/read_targets.R")
library(seqLogo)
source("~/ll-utils/rfast/utils.R")
source("~/ll-utils/array/normalize.R")
source("~/ll-utils/GO/overlap.R")
source("~/ll-utils/GO/go.R")
source("~/ll-utils/gwas/enrich.R")

source("~/ll-utils/gwas/plink.R")
source("~/ll-utils/gwas/enrich.R")
source("~/ll-utils/gwas/io.R")
source("~/ll-utils/rfast/utils.R")


main <- function() {
  stop("here")

  source("~/unagidata/foxp3/seq_process.R")
  source("~/ll-utils/chromatin/genes.R")
  size.clust <- 5
  show(sprintf("Number of processors to use: %d", size.clust))
  library(snow)
  cluster <- makeCluster(size.clust,type="SOCK")
  
  get.all.peaks(cluster)
  get.uniform.peaks(cluster)
  get.pretty.heatmaps(cluster)
  get.all.joints(cluster)
  find.sequence.signal(cluster)
  get.track.plots(cluster)
  check.stat3.foxp3.binding(cluster)
}
show("hello")






get.big.table <- function (expmnts, npeaks, run.name, peaks=NULL) {
  all.itr <- peaks
  if (is.null(peaks)) {
    peaks <- read.peaks(files[expmnts], npeaks) 
    all.itr <- get.overlap.genome(peaks, "union")
  }
  
  num.regions(all.itr)
  pos <- itr.to.regions(all.itr)
  joint.reads <- df.to.reads(pos, reads)
  joint.reads <- joint.reads / ((pos$end - pos$start)) * mean(pos$end - pos$start)
  
  pos <- strloc.to.loc(rownames(joint.reads))
  mid <- round(rowMeans(cbind(pos$start, pos$end)))
  pos$start <- mid-1000
  pos$end <- mid+1000
  neg.pos <- pos; neg.pos$start <- neg.pos$start + 300; neg.pos$end <- neg.pos$end + 300
  tmp <- loc2seq(rbind(pos, neg.pos), run.python=T, genome.dir="/unagidata/leslielab/local/share/genomes/mm9/")
  long.pos.seqs <- tmp[1:nrow(pos)]
  names(long.pos.seqs) <- rownames(joint.reads)
  long.neg.seqs <- tmp[(nrow(pos)+1):length(tmp)]
  seqs <- substr(long.pos.seqs, 900, 1100)
  neg.seqs <- substr(long.neg.seqs, 900, 1100)
  orig.seqs <- seqs
  orig.neg.seqs <- neg.seqs
  
  pos$centers <-  round(rowMeans(cbind(pos$start, pos$end)))
  pos$score <- 0
  m <- location.to.genedata(pos, "mm9", skip.go=T)
  m$GeneSymbol <- toupper(m$GeneSymbol)
  
  ncel.file <- "Robbieexpression/ncel.rda"
  vars <- load(ncel.file)
  expr <- exprs(ncel)
  gi <- probeset.to.geneinfo(rownames(expr), sprintf("%s.db", annotation(ncel))) ##"mouse4302.db")
  gi$name <- toupper(gi$name)
  
  
  source("~/ll-utils/array/normalize.R")
  sets <- c("ATe", "ATreg", "KIKOhet", "KIKOsick", "RTn", "Rtreg")
  array.fnames <- sampleNames(ncel)
  design <- rep(0,length(array.fnames))
  for (i in 1:length(sets)) {
    set <- sampleNames(ncel)[grep(sets[i], sampleNames(ncel))]
    idx <- which(array.fnames %in% set)
    for (j in idx) {design[j] <- i}
  }
  design <- model.matrix(~ -1+factor(design))
  colnames(design) <- cnames <- sets
  fit <- lmFit(ncel, design)
  
  contrast.names <- c()
  for (i in 1:(length(cnames)-1)) {
    for (j in (i+1):length(cnames)) {
      contrast.names <- c(contrast.names, sprintf("%s-%s", cnames[i], cnames[j]))
    }
  }
  show(contrast.names)
  
  contrast <- makeContrasts(contrasts=contrast.names, levels=design)
  fit2 <- eBayes(contrasts.fit(fit, contrast))
  
  diff.genes <- cbind(fit2$coefficients, fit2$t, fit2$p.value)
  colnames(diff.genes) <- c(paste("Diff-", colnames(fit2$coefficients), sep=""),
                            paste("T-", colnames(fit2$t), sep=""),
                            paste("p-", colnames(fit2$p.value), sep=""))
  out <- cbind(gi, diff.genes)
  write.table(out, file="diff_expr.txt")
  
  
  ugenes <- toupper(unique(m$GeneSymbol))
  gene2expr <- list()
  aaa <- 1
  for (i in 1:length(ugenes)) {
    if (i %% 500 == 0)
      show(i)
    g <- ugenes[i]
    idx <- which(gi$name==g)
    if (length(idx) == 0) {gene2expr[[g]] <- NA; next }
    
    if (length(idx) == 1){ gene2expr[[g]] <- c(expr[idx,], diff.genes[idx,]); next }
    
    dg <- c()
    dg <- c(dg, apply(diff.genes[idx, grep("^Diff\\-", colnames(diff.genes))], 2, function(x){x[which(abs(x)==max(abs(x)))]}))
    dg <- c(dg, apply(diff.genes[idx, grep("^T\\-", colnames(diff.genes))], 2, function(x){x[which(abs(x)==max(abs(x)))]}))
    dg <- c(dg, apply(diff.genes[idx, grep("^p\\-", colnames(diff.genes))], 2, function(x){x[which(abs(x)==min(abs(x)))]}))
    names(dg) <- colnames(diff.genes)
    gene2expr[[g]] <- c(colMeans(expr[idx,]), dg)
  }
  
  m <- cbind(m, matrix(-99, nrow=nrow(m), ncol=ncol(expr)+ncol(diff.genes)))
  colnames(m)[(ncol(m)-ncol(expr)-ncol(diff.genes)+1):ncol(m)] <- c(colnames(expr), colnames(diff.genes))
  for (i in 1:nrow(m)) {
    if (i %% 500 == 0)
      show(i)
    g <- m$GeneSymbol[i]
    if (is.null(gene2expr[[g]])) { m[i, (ncol(m)-ncol(expr)-ncol(diff.genes)+1):ncol(m)] <- NA; next; }
    m[i, (ncol(m)-ncol(expr)-ncol(diff.genes)+1):ncol(m)] <- gene2expr[[g]]
  }
  
  
  dim(joint.reads)
  dim(expr)
  dim(diff.genes)
  dim(m)
  out <- cbind(m, joint.reads)





  write.table(out, sprintf("bigtable_%s.txt", run.name), sep="\t")
}



get.primers <- function() {
  f <- read.fasta("chip-primers")
  primers <- list()
  for (i in 1:length(f$seqs)) 
    primers[[i]] <- try(find.primers(toupper(f$seqs[i])))


  out <- data.frame(name=sapply(strsplit(f$names,"name="), function(x){x[2]}),
                    left=sapply(primers, function(x){x[[1]]$left}) ,
                    right=sapply(primers, function(x){x[[1]]$right}) ,
                    left.start=sapply(primers, function(x){x[[1]]$left.start}) ,
                    right.start=sapply(primers, function(x){x[[1]]$right.start}) ,
                    nchar=nchar(f$seq) )
  out


  out <- data.frame(name=sapply(strsplit(f$names,"name="), function(x){x[2]}),
                   left=sapply(primers, function(x){x[[2]]$left}) ,
                   right=sapply(primers, function(x){x[[2]]$right}) ,
                   left.start=sapply(primers, function(x){x[[2]]$left.start}) ,
                   right.start=sapply(primers, function(x){x[[2]]$right.start}),
                   nchar=nchar(f$seq))
  out
  
  out <- data.frame(name=sapply(strsplit(f$names,"name="), function(x){x[2]}),
                    left=sapply(primers, function(x){x[[3]]$left}) ,
                    right=sapply(primers, function(x){x[[3]]$right}) ,
                    left.start=sapply(primers, function(x){x[[3]]$left.start}) ,
                    right.start=sapply(primers, function(x){x[[3]]$right.start}) ,
                    nchar=nchar(f$seq) )
  out


  dual
  f <- list()
  f <- c(f, list(list(gene="rps26", loc="chr12:56,435,611-56,436,075")))
  f <- c(f, list(list(gene="MTRF", loc="chr13:41,837,461-41,837,919")))
  f <- c(f, list(list(gene="fancm", loc="chr14:45,604,988-45,605,410")))
  f <- c(f, list(list(gene="NCLN", loc="chr19:3,185,635-3,186,057")))
  f <- c(f, list(list(gene="rpl32", loc="chr3:12,882,821-12,883,540")))
  
  ets1
  ##f <- list()
  f <- c(f, list(list(gene="ctf1", loc="chr16:30,908,330-30,908,758")))
  f <- c(f, list(list(gene="znf235", loc="chr19:44,808,953-44,809,285")))
  f <- c(f, list(list(gene="rad52", loc="chr12:1,058,554-1,058,928")))
  f <- c(f, list(list(gene="flj14397", loc="chr2:74,709,632-74,710,150")))
  
  
  
  elf1
  ##f <- list()
  f <- c(f, list(list(gene="dlx3", loc="chr17:48,075,566-48,076,111")))
  f <- c(f, list(list(gene="fbxl6", loc="chr16:67,193,752-67,194,171")))
  f <- c(f, list(list(gene="nxph4", loc="chr12:57,607,658-57,608,086")))
  f <- c(f, list(list(gene="mgc12981", loc="chr2:131,099,720-131,100,157")))
  f <- c(f, list(list(gene="unknown", loc="chr2:131,100,136-131,100,573")))
  
  


  source("~/ll-utils/ucsc/liftover.R")
  f
  pos <- strloc.to.loc(sapply(f, function(x){gsub(",", "", x$loc)}))
  pos$start <- pos$end <- (pos$start + pos$end)/2
  pos$start <- pos$start - 2
  pos$end <- pos$end + 2
  pos
  pos <- run.liftover(cbind(pos, strand="+"), "~/ll-utils/ucsc/hg19ToMm9.over.chain")
  pos

  pos$start <- pos$start - 200
  pos$end <- pos$end + 200
  fasta <- loc2seq(pos, bsgenome.package="BSgenome.Mmusculus.UCSC.mm9", as.char=T)  
  
  primers <- list()
  for (i in 1:length(fasta)) 
    primers[[i]] <- try(find.primers(fasta[i]))
  for (i in 1:length(primers)) {
    if (is.object(primers[[i]]) && is(primers[[i]], "try-error")) {
      show(i)
      primers[[i]] <- primers[[1]]
      primers[[i]][[1]]$left <- primers[[i]][[1]]$right <- ""
      primers[[i]][[1]]$left.start <- primers[[i]][[1]]$right.start <- 0
    }
  }
  names <- sapply(f, function(x){x$gene})
  out <- data.frame(name=names, 
                    left=sapply(primers, function(x){x[[1]]$left}) ,
                    right=sapply(primers, function(x){x[[1]]$right}) ,
                    left.start=sapply(primers, function(x){x[[1]]$left.start}) ,
                    right.start=sapply(primers, function(x){x[[1]]$right.start}) ,
                    seq=fasta)
  write.table(out, "misc_primers.txt", col.names=NA, quote=F, sep="\t")


  cat(c(paste(out$name, out$left, sep="_left;"), paste(out$name, out$right, sep="_right;")), sep=";\n")
  
  

  
  f <- c(f, list(list(gene="
", loc="
")))
  
  f <- c(f, list(list(gene="
", loc="
")))
  


  
}




















idr.to.loc <- function(obj, use.idr=F, use.local.idr=F, use.raw=F, use.qn=F, idr=10, cutoffs=c(-1,-1)) { 
  show(round(quantile(obj$joint$joint$mat[,1], seq(0,1,0.05)), 2))
  show(round(quantile(obj$joint$joint$mat[,2], seq(0,1,0.05)), 2))
  show(obj$idr.stats)

  pos.raw <- obj$joint$locations[which(obj$joint$joint$mat[,1] > cutoffs[1] &
                                       obj$joint$joint$mat[,2] > cutoffs[2]), ]

  if (use.local.idr)  cutoff <- idr
  if (use.idr)  cutoff <- obj$idr.stats[which(obj$idr.stats[,"IDR.cutoff"] < idr), "local.idr.cutoff"]
  pos.idr <- obj$joint$locations[which(obj$idr.res$em.fit$e.z > 1-cutoff), ]
  return(list(pos.raw=pos.raw, pos.idr=pos.idr))
}

trim.range <- function(tmp, cutoff=0.02) {
    mz <- quantile(tmp,1-cutoff);     tmp[which(tmp>mz)] <- mz
    mz <- quantile(tmp,cutoff);     tmp[which(tmp<mz)] <- mz
    return(tmp)
}


quick.auc <- function(preds, y) {
  x1 = preds[y>0]
  n1 = length(x1)
  x2 = preds[y<=0]
  n2 = length(x2)
  r = rank(c(x1,x2))
  auc = (sum(r[1:n1]) - n1*(n1+1)/2) / (n1*n2)
  return(auc)
}






