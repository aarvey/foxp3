source("~/.path.R")
library(spp)
library(snow)
library(BSgenome.Mmusculus.UCSC.mm9)
source("~/ll-utils/aligners/align2peak.R")
source("~/ll-utils/ucsc/formats.R")
source("~/ll-utils/chromatin/itr.R")
source("~/ll-utils/conservation/getcons.R")
library(snow)

source("~/ll-utils/array/normalize.R")
library(mouse430a2.db)
expr.data <- read.table("data/miR155/miR155.exprs.csv", sep=",", header=T)
gene.info <- probeset.to.geneinfo(expr.data[,1], "mouse430a2.db")
expr.data.ko <- read.table("data/KO/KO.exprs.csv", sep=",", header=T)
expr.data <- cbind(gene.info, expr.data, expr.data.ko)
genename.to.expridx <- as.list(rep(0, length(unique(gene.info[,"name"]))))
names(genename.to.expridx) <- unique(gene.info[,"name"])
for (i in 1:nrow(gene.info)) {
  genename.to.expridx[[gene.info[i,"name"]]] <- c(genename.to.expridx[[gene.info[i,"name"]]], i)
  if (i %% 1000 == 0) { show(i)}
}
genename.to.expridx <- sapply(genename.to.expridx, function(x){x[-1]})
write.table(expr.data, file=sprintf("data/expr.txt"), sep="\t", quote=F, col.names=T, row.names=F)
wt.expr.cols <- grep("GL_wt", colnames(expr.data))
ko.expr.cols <- grep("GL_mir155KO", colnames(expr.data))


main <- function() {
  size.clust <- 12
  show(sprintf("Number of processors to use: %d", size.clust))
  if (size.clust>1)  {
    cluster <- makeCluster(size.clust,type="SOCK")
  } else {
    cluster <- NULL
  }
  
  files <- list()
  files[[length(files)+1]] <- list()
  files[[length(files)]]$fa.fname <- "wt_ago.aly.fa.notag.align.uniquetags"
  files[[length(files)]]$fa.root <- "wt_ago"
  
  files[[length(files)+1]] <- list()
  files[[length(files)]]$fa.fname <- "wt_2.aly.fa.notag.align.uniquetags"
  files[[length(files)]]$fa.root <- "wt_2"
  
  files[[length(files)+1]] <- list()
  files[[length(files)]]$fa.fname <- "1000.aly.fa.notag.align.uniquetags"
  files[[length(files)]]$fa.root <- "1000"
  
  files[[length(files)+1]] <- list()
  files[[length(files)]]$fa.fname <- "20000.aly.fa.notag.align.uniquetags"
  files[[length(files)]]$fa.root <- "20000"
  
  files[[length(files)+1]] <- list()
  files[[length(files)]]$fa.fname <- "s7.aly.fa.notag.align.uniquetags"
  files[[length(files)]]$fa.root <- "s7"
  
  #files[[length(files)+1]] <- list()
  #files[[length(files)]]$fa.fname <- "wt.fa.notag.align.uniquetags"
  #files[[length(files)]]$fa.root <- "wt"
  
  #files[[length(files)+1]] <- list()
  #files[[length(files)]]$fa.fname <- "155.fa.notag.align.uniquetags"
  #files[[length(files)]]$fa.root <- "155"

  for (i in 1:length(files)) {
    hits.clip.process(files[[i]]$fa.fname, files[[i]]$fa.root, cluster)
  }
  
}


hela.main <- function() {
  fa.fnames <- dir(pattern=".notag.bowtie.unmapped.viral.bowtie.align.uniquetags$")
  fa.root <- "combined_viral"
  hits.clip.process(fa.fnames, fa.root, cluster, get.gene.data=F)


  fa.fnames <- dir(pattern=".notag.bowtie.align.uniquetags$")
  fa.root <- "combined"
  hits.clip.process(fa.fnames, fa.root, cluster, get.gene.data=F)
  
}


hits.clip.process <-  function(fa.fnames, fa.root, cluster, get.gene.data=T) {

  file.reader <- read.bowtie.tags
  hc.data <- list()
  bed.data <- read.table(fa.fnames[1], header=F, sep="\t", nrows=1)
  for (fa.fname in fa.fnames) {
    hcd <- file.reader(fa.fname)$tags
    for (chr in names(hcd)) {
      hc.data[[chr]] <- sort(c(hc.data[[chr]], hcd[[chr]]))
    }
    bed.data <- rbind(bed.data, read.table(fa.fname, header=F, sep="\t"))
  }
  bed.data <- bed.data[-1, ]
  ##write.bed(hc.data, sprintf("%s_rawreads.bed", fa.root), 20, sprintf("%s_ATReg_HITS_CLIP", fa.root), sprintf("%s_ATReg_HITS_CLIP", fa.root))
  ##TTTTG   +       chr13   67101640        TTTGCTTTTCCTCTTACCCTGGTGGCCTGT  IIIIIIIIIIIIIIIIIIIIIIIIIIIIII  0       28:C>G,29:C>T

  colnames(bed.data) <- c("id", "strand", "chr", "start", "seq", "qual", "nalign", "mismatch")
  start = as.numeric(bed.data[,"start"])
  end = start
  strand = bed.data[,"strand"]
  len = sapply(bed.data[,"seq"], nchar)
  names(len) <- NULL
  pos.idx <- which(strand=="+")
  neg.idx <- which(strand=="-")
  end[pos.idx] <- start[pos.idx] + len[pos.idx]  
  end[neg.idx] <- start[neg.idx] + len[neg.idx]  
  bed.df <- data.frame(chr=bed.data[,"chr"], start=start, end=end, strand=strand)
  source("~/ll-utils/ucsc/formats.R")
  write.bed(bed.df, sprintf("%s_rawreads.bed", fa.root), 20, sprintf("%s_ATReg_HITS_CLIP", fa.root), sprintf("%s_ATReg_HITS_CLIP", fa.root), is.data.frame=T)
  
  
  tag.shift <- 0
  bandwidth <- 20
  step.size <- 5
  
  smoothed.density <- get.smoothed.tag.density(hc.data,bandwidth=bandwidth,step.size=step.size,tag.shift=tag.shift,cluster=cluster);
  names(smoothed.density) <- names(hc.data)
  ##writewig.by.chrom(smoothed.density, sprintf("%s_smoothed_%dbp", fa.root, step.size), sprintf("%s HITS-CLIP smoothed signal", fa.root), cluster)
  
  
  get.peak.mat <- function(threshold) {
    source("~/ll-utils/aligners/align2peak.R")
    show(threshold)
    peak.mat <- get.smoothed.density.peaks(smoothed.density, threshold, min.size=step.size)
    show(dim(peak.mat))
    peak.fname <- sprintf("%s_tagshift%d_bandwidth%d_stepsize%d_threshold%d_clusters", fa.root,
                          tag.shift, bandwidth, step.size, threshold)
    write.table(peak.mat, file=sprintf("%s.txt",peak.fname), sep="\t", quote=F, col.names=T, row.names=F)
    return(0)
  }
  thresholds <- c(3,4,5,7,10, 15, 20)
  thresholds <- c(3)
  ##clusterExport(cluster, list("smoothed.density", "step.size", "fa.root", "tag.shift", "bandwidth"))
  ##clusterApplyLB(cluster, thresholds, get.peak.mat)
  sapply(thresholds, get.peak.mat)
  
  threshold <- 3
  peak.fname <- sprintf("%s_tagshift%d_bandwidth%d_stepsize%d_threshold%d_clusters", fa.root,
                        tag.shift, bandwidth, step.size, threshold)
  peak.mat <- read.table(sprintf("%s.txt",peak.fname), header=T)
  
  strand <- c()
  for (i in 1:nrow(peak.mat)) {
    s <- peak.mat[i,"start"]
    e <- peak.mat[i,"end"]
    chr <- peak.mat[i,"chr"]
    idx <- which(abs(hc.data[[chr]]) > s-25 & abs(hc.data[[chr]]) < e+25)
    tags <- hc.data[[chr]][idx]
    ps.ratio <- length(which(tags >=0))  / length(which(tags <0))
    if (length(ps.ratio)==0 || !(ps.ratio > 0.98 || ps.ratio < 0.02)) {
      strand[i] <- "?"
      show("Confused!")
    } else if (tags[1]>0) {
      strand[i] <- "+"
    } else if (tags[1]<0) {
      strand[i] <- "-"
    }
  }
  peak.mat <- cbind(peak.mat, strand)
  

  if (!get.gene.data) {return()}
  
  
  peak.itr <- read.genome.itr(x=peak.mat, organism="mm")
  for (n in intersect(names(peak.itr$other.cols), names(peak.itr$itr))) {
    peak.itr$itr[[n]][,1] <- peak.itr$other.cols[[n]][,"start"]
    peak.itr$itr[[n]][,2] <- peak.itr$other.cols[[n]][,"end"]
  }
  
  itr <- peak.itr$itr
  vals <- unlist(lapply(peak.itr$other.cols, function(x){x[,"val"]}))
  
  exon.intron.itr <- get.exon.intron.itr(organism="mm9")
  key2feats <- list()
  for (key in names(exon.intron.itr)) {
    show(key)
    key2feats[[key]] <- get.features(itr, exon.intron.itr[[key]])
  }
  
  key2feats[["stream"]] <- sapply(1:length(key2feats[["tss.itr"]]),
                                  function(i) {
                                    x <- key2feats[["tss.itr"]][i]
                                    y <- key2feats[["promoter.itr"]][i]
                                    z <- x - y
                                    if(y == 0) return("up")
                                    if(abs(z)==1000) return("up")
                                    if(z==0) return("down")
                                    return("middle")
                                  }
                                  )
  
  
  cons.dir <- "/unagidata/leslielab/local/share/conservation/mm9"
  cons.mat <- cbind(as.character(peak.mat[,"chr"]), peak.mat[,"start"],  peak.mat[,"end"], "+")
  colnames(cons.mat) <- c("chr","start","end","strand")
  cons <- get.conservation(cons.mat, sprintf("%speakcons.txt", fa.root), cons.dir)
  cons.max <- sapply(cons, max)
  
  center.peak.itr <- read.genome.itr(fname=NULL, is.positions=T, organism="mm", x=cbind(peak.mat, cons.max))
  cons.feats <- unlist(sapply(center.peak.itr$other.cols, function(x){x$cons.max}))
  key2feats[["cons.itr"]] <- as.numeric(as.character(cons.feats))
  
  
  
  
  
  dim(peak.mat)
  num.regions(peak.itr$itr)
  sapply(key2feats, length)
  sapply(key2feats, function(x){as.numeric(x[1:10])})
  length(which(key2feats[["intron.itr"]] > 50 & key2feats[["exon.itr"]] > 50 & key2feats[["utr5.itr"]] > 50 & key2feats[["utr3.itr"]] > 50))
  length(which(key2feats[["utr3.itr"]]==0))
  length(which(key2feats[["utr5.itr"]]==0))
  length(which(key2feats[["intron.itr"]]==0))
  length(which(key2feats[["promoter.itr"]]==0))
  length(which(key2feats[["gene.itr"]]==0))
  length(which(key2feats[["gene.itr"]]>0))
  length(which(key2feats[["gene.itr"]]>100))
  length(which(key2feats[["gene.itr"]]>1000))
  length(which(key2feats[["gene.itr"]]>1000 & key2feats[["promoter.itr"]]>1000))
  length(which(key2feats[["stream"]]=="down" & key2feats[["gene.itr"]] > 100 & as.numeric(cons.max) > 0.97))
  
  
  rois  <- list()
  rois[[length(rois)+1]] <- list()
  rois[[length(rois)]]$peak.idx <- which(key2feats[["gene.itr"]] > 100)
  rois[[length(rois)]]$fname.root <- "outside_refseq"
   
  rois[[length(rois)+1]] <- list()
  rois[[length(rois)]]$peak.idx <- which(key2feats[["intron.itr"]] < 10 & key2feats[["utr3.itr"]] > 10 & key2feats[["utr5.itr"]] > 10)
  rois[[length(rois)]]$fname.root <- "intronic"
  
  rois[[length(rois)+1]] <- list()
  rois[[length(rois)]]$peak.idx <- 1:length(key2feats[["gene.itr"]])
  rois[[length(rois)]]$fname.root <- "allpeaks"
  
  rois[[length(rois)+1]] <- list()
  rois[[length(rois)]]$peak.idx <- which(key2feats[["gene.itr"]] > 10 & (key2feats[["utr3.itr"]] < 2000 | key2feats[["utr5.itr"]] < 2000 | key2feats[["gene.itr"]] < 2000))
  rois[[length(rois)]]$fname.root <- "outside_refseq_near_utr"
  
  
  for (roi in rois) {
    peak.idx <- roi$peak.idx
    fname.root <- roi$fname.root
    nc.vals <- rep(0,length(peak.idx))
    cnames <- c("Gene", "RefSeq", "GeneUTR", "RefSeqUTR", "Reads", "Loc", "GeneDist",
                "UTR3PDist", "UTR5PDist", "IntronDist", "TSSDist", "TTSDist", "phastCons", "GeneUTRExpr", "GeneUTRExpr_KO155")
    roi.mat <- matrix("", nrow=length(peak.idx), ncol=length(cnames))
    colnames(roi.mat) <- cnames
    for(i in 1:length(peak.idx)) {
      pi <- peak.idx[i]
      for(chr in names(itr)) {
        pi <- pi - nrow(itr[[chr]])
        if (pi < 1) {
          pi <- pi + nrow(itr[[chr]])
          break
        }
      }
      nc.vals[i] <- peak.itr$other.cols[[chr]][pi,"val"]
      loc <- sprintf("%s:%d-%d(%s)", peak.itr$other.cols[[chr]][pi,"chr"],
                     peak.itr$other.cols[[chr]][pi,"start"], peak.itr$other.cols[[chr]][pi,"end"],
                     peak.itr$other.cols[[chr]][pi,"strand"])
      names(nc.vals)[i] <- loc
      gname <- names(key2feats[["gene.itr"]][peak.idx[i]])
      gene.name <- strsplit(gname, "---")[[1]][1]
      refseq.name <- strsplit(gname, "---")[[1]][2]


      utr.gname <- names(key2feats[["tts.itr"]][peak.idx[i]])
      utr.gene.name <- strsplit(utr.gname, "---")[[1]][1]
      utr.refseq.name <- strsplit(utr.gname, "---")[[1]][2]
      expr.idx <- genename.to.expridx[[utr.gene.name]]
      wt.gene.expr <- NA
      ko.gene.expr <- NA
      if (!is.null(expr.idx)) {
        wt.gene.expr <- mean(unlist(expr.data[expr.idx,wt.expr.cols]))
        ko.gene.expr <- mean(unlist(expr.data[expr.idx,ko.expr.cols]))
      }
      roi.mat[i,] <- c(gene.name, refseq.name,
                       utr.gene.name, utr.refseq.name,
                       peak.itr$other.cols[[chr]][pi,"val"],
                       loc,
                       key2feats[["gene.itr"]][peak.idx[i]],
                       key2feats[["utr3.itr"]][peak.idx[i]],
                       key2feats[["utr5.itr"]][peak.idx[i]],
                       key2feats[["intron.itr"]][peak.idx[i]],
                       key2feats[["tss.itr"]][peak.idx[i]],
                       key2feats[["tts.itr"]][peak.idx[i]],
                       key2feats[["cons.itr"]][peak.idx[i]],
                       wt.gene.expr, ko.gene.expr)
    }
    sort(nc.vals)
    roi.mat <- roi.mat[order(nc.vals, decreasing=T), ]
    write.table(roi.mat, file=sprintf("%s_%s_peaks.txt", fa.root, fname.root), sep="\t", quote=F, col.names=T, row.names=F)
  } 
}


main()




##
## Look at differences in peak calls
##

#peaks.wt <- read.table("wt_outside_refseq_peaks.txt", sep="\t", header=T)
#peaks.155 <- read.table("155_outside_refseq_peaks.txt", sep="\t", header=T)
#peaks.wt$Gene %in% peaks.155$Gene















check.gabe.seqs <- function() {
    # source("~/unagidata/foxp3/seq_process.R")
    ##seqs <- read.fasta("gabe_seqs1.txt")$seqs
    ##ret <- get.all.motif.analysis.hexamer.analysis(seqs)

    pos.fasta <- read.fasta("gabe_seqs_ntdel.txt")
      n <- strsplit(pos.fasta$names,",")
      n <- as.numeric(sapply(n, "[", 3))
      seqs <- pos.fasta$seqs[which(n > 20)]

      neg.fasta <- read.fasta("gabe_seqs_ntdel_upstream.txt")
      neg.seqs <- sample(neg.fasta$seqs, length(seqs))

      run.name <- "gabe"

      test.seqs <- setdiff(pos.fasta$seqs[which(n > 15)], seqs)
      test.seqs <- test.seqs[which(test.seqs > 12)]
      test.neg.seqs <- sample(setdiff(neg.fasta$seqs, neg.seqs), length(test.seqs))
  }


