import sw
import sys


if __name__ == '__main__':
    similarityMatrixMap = sw.readBLOSUM("blosum62.txt")
    travs_fname = sys.argv[1]
    f = open(travs_fname)
    travs = [l.strip().split() for l in f.readlines()]
    f.close()
    
    tcr_counts_fname = sys.argv[2]
    f = open(tcr_counts_fname)
    tcrs = [l.strip().split() for l in f.readlines()]
    f.close()

    output_all = False
    if len(sys.argv) > 3:
        output_all = int(sys.argv[3])
        
    print len(tcrs), ' tcrs'
    tcrs = [tcr for tcr in tcrs if int(tcr[0]) > 1]
    print len(tcrs), ' tcrs with multiple counts'

    gap_penalty = -20
    trav_idx = 2
    print len(travs), ' travs'
    if output_all:
        trav_idx = 1
        travs = [trav for trav in travs if int(trav[0]) > 1]
        print len(travs), ' travs with multiple counts'
        

    for i in range(len(tcrs)):
        seq1 = tcrs[i][1]
        counts = int(tcrs[i][0])
        ##print seq1
        maxScore = -99999999
        for j in range(len(travs)):
            seq2 = travs[j][trav_idx]
            ##print seq2
            score = sw.computeFMatrix(seq1, seq2, gap_penalty, similarityMatrixMap, False)
            if output_all: print i, j, score            
            if score > maxScore:
                maxScore = score
                maxTrav = seq2
        print 'Max', i, counts, seq1, maxTrav, maxScore
        







