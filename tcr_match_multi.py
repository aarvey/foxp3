import sw
import sys
import threading
import Queue



THREAD_LIMIT = 72                # This is how many threads we want
jobs = Queue.Queue(30)           # This sets up the queue object to use 5 slots


similarityMatrixMap = sw.readBLOSUM("blosum62.txt")
travs_fname = sys.argv[1]
f = open(travs_fname)
travs = [l.strip().split() for l in f.readlines()]
f.close()

tcr_counts_fname = sys.argv[2]
f = open(tcr_counts_fname)
tcrs = [l.strip().split() for l in f.readlines()]
f.close()

print len(travs), ' travs'
print len(tcrs), ' tcrs'

tcrs = [tcr for tcr in tcrs if int(tcr[0]) > 2]
print len(tcrs), ' tcrs with multiple counts'

results = [(0,0,'','',0)  for i in range(len(tcrs))]

gap_penalty = -20

class workerbee(threading.Thread):
    def run(self):
        # run forever
        while 1:
            # Try and get a job out of the queue
            try:
                job = jobs.get(True,1)
                ##print "Multiplication of {0} with {1} gives {2}".format(job[0],job[1],(job[0]*job[1]))
                i = job
                print i
                count = tcrs[i][0]
                seq1 = tcrs[i][1]
                maxScore = -99999999
                for j in range(len(travs)):
                    seq2 = travs[j][2]
                    score = sw.computeFMatrix(seq1, seq2, gap_penalty, similarityMatrixMap, False)
                    if score > maxScore:
                        maxScore = score
                        maxTrav = seq2
                results[i] = (i,count, seq1, maxTrav, maxScore)
                print i, 'done'
                if False:
                    singlelock.acquire()        # Acquire the lock
                    print i, 'TCR (', count, ') :', seq1, ' has trav', maxTrav, maxScore
                    singlelock.release()        # Release the lock
                # Let the queue know the job is finished.
                jobs.task_done()
            except:
                break           # No more jobs in the queue




if False:
    def workerbee(i):    print i
    count = tcrs[i][0]
    seq1 = tcrs[i][1]
    maxScore = -99999999
    for j in range(len(travs)):
        seq2 = travs[j][2]
        score = sw.computeFMatrix(seq1, seq2, gap_penalty, similarityMatrixMap, False)
        if score > maxScore:
            maxScore = score
            maxTrav = seq2
    print i, 'done'
    results[i] = (i,count, seq1, maxTrav, maxScore)

    import thread,time
    for i in range(len(tcrs)): print i
    thread.start_new(workerbee, (i,))
    time.sleep(0.5)
    time.sleep(1000)

# Spawn the threads
print "Spawning the {0} threads.".format(THREAD_LIMIT)
for x in xrange(THREAD_LIMIT):
    print "Thread {0} started.".format(x)
    workerbee().start()
    
# Put stuff in queue
print "Putting stuff in queue"
for i in range(len(tcrs)):
    try:
        jobs.put(i, block=True, timeout=5)
    except:
        print "The queue is full !"
print 'finished with all worker bees'            
print "Waiting for threads to finish."
jobs.join()                 # This command waits for all threads to finish.


            

sys.exit(0)


if __name__ == '__main__':
    similarityMatrixMap = sw.readBLOSUM("blosum62.txt")
    travs_fname = sys.argv[1]
    f = open(travs_fname)
    travs = [l.strip().split() for l in f.readlines()]
    f.close()
    
    tcr_counts_fname = sys.argv[2]
    f = open(tcr_counts_fname)
    tcrs = [l.strip().split() for l in f.readlines()]
    f.close()

    print len(travs), ' travs'
    print len(tcrs), ' tcrs'

    tcrs = [tcr for tcr in tcrs if int(tcr[0]) > 2]
    print len(tcrs), ' tcrs with multiple counts'

    gap_penalty = -20
    for i in range(len(tcrs)):
        seq1 = tcrs[i][1]
        ##print seq1
        maxScore = -99999999
        for j in range(len(travs)):
            seq2 = travs[j][2]
            ##print seq2
            score = sw.computeFMatrix(seq1, seq2, gap_penalty, similarityMatrixMap, False)
            if score > maxScore:
                maxScore = score
                maxTrav = seq2
        print i, 'TCR:', seq1, ' has trav ', maxTrav, ' with score ', maxScore
        continue
        (alignedSeq1, alignedSeq2, alignPointer) = sw.computeFMatrix(seq1, maxTrav, gap_penalty, similarityMatrixMap, True)
        print alignedSeq1
        print alignPointer
        print alignedSeq2
        ##if i > 3: break

    exit(0)


