
FILEZILLA: 
lftp ftp-private.ebi.ac.uk
set ftp:ssl-force yes
set ftp:ssl-protect-data no
set ssl:verify-certificate no
ls



nohup ~/.aspera/connect/bin/ascp -QT -l640M  -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:MS .
~/.aspera/connect/bin/ascp -QT -l640M ega-box-173@fasp.ega.ebi.ac.uk:WTCCC2_NBS/README.txt.gpg .

~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:T2D/Affymetrix500K     .
~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:T2D/imputed     .
~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:T2D/Affymetrix500K/Affx_20060707fs1_SNP_WTCCC1_T2D.txt.gz.gpg     . 

~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:RA/Affymetrix500K     .
~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:RA/imputed     .


nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:T1D/Affymetrix500K   .    < ~/unagidata/foxp3/wtcc_ega.txt &
nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:T1D/imputed   .    < ~/unagidata/foxp3/wtcc_ega.txt &
nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:T1D/HLA   .    < ~/unagidata/foxp3/wtcc_ega.txt &

nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:TB_AFC   .    < ~/unagidata/foxp3/wtcc_ega.txt &

nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:PD   .    < ~/unagidata/foxp3/wtcc_ega.txt &

nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:IS/ilmn670_ccc2_is.idat.tar.gz.gpg   .    < ~/unagidata/foxp3/wtcc_ega.txt &
nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:IS/WTCCC2_IS_EGA_10112011.tar.gpg  .    < ~/unagidata/foxp3/wtcc_ega.txt &
nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:IS/Human670-QuadCustom_v1_A.csv.gpg  .    < ~/unagidata/foxp3/wtcc_ega.txt &
nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:IS/WTCCC2_IS_README.txt.gpg  .    < ~/unagidata/foxp3/wtcc_ega.txt &
nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:IS/md5sum.txt.gpg  .    < ~/unagidata/foxp3/wtcc_ega.txt &

nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:HT/Affymetrix500K   .    < ~/unagidata/foxp3/wtcc_ega.txt &
nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:HT/imputed   .    < ~/unagidata/foxp3/wtcc_ega.txt &

nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:AS   .    < ~/unagidata/foxp3/wtcc_ega.txt &

nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:BC   .    < ~/unagidata/foxp3/wtcc_ega.txt &

nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:BD/Affymetrix500K   .    < ~/unagidata/foxp3/wtcc_ega.txt &
nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:BD/imputed   .    < ~/unagidata/foxp3/wtcc_ega.txt &

nohup ~/.aspera/connect/bin/ascp -QT -l640M -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:CAD/Affymetrix500K   .    < ~/unagidata/foxp3/wtcc_ega.txt &

nohup ~/.aspera/connect/bin/ascp -QT -l640M -E CEL -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:1958BC   .    < ~/unagidata/foxp3/wtcc_ega.txt &
nohup ~/.aspera/connect/bin/ascp -QT -l640M -E CEL -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:EGAS00000000031   .    < ~/unagidata/foxp3/wtcc_ega.txt &
nohup ~/.aspera/connect/bin/ascp -QT -l640M -E CEL -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:EGAS00000000026_Controls  .    < ~/unagidata/foxp3/wtcc_ega.txt &
nohup ~/.aspera/connect/bin/ascp -QT -l640M -E CEL -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:EGAS00000000026_Cases  .    < ~/unagidata/foxp3/wtcc_ega.txt &


nohup ~/.aspera/connect/bin/ascp -QT -l640M -E CEL -i asperaweb_id_dsa.putty ega-box-173@fasp.ega.ebi.ac.uk:EGAD00010000250  .    < ~/unagidata/foxp3/wtcc_ega.txt &


nohup ~/.aspera/connect/bin/ascp -QTr -l 300M -k 1 -i asperaweb_id_dsa.putty -W   AACEF535245EF2345345AAFA6800DE771AC61D dbtest@gap-upload.ncbi.nlm.nih.gov:data/instant/rudensky/28730 . &



nohup ~/.aspera/connect/bin/ascp -QTr -l 1000M -k 1 -i asperaweb_id_dsa.putty ega-box-78@fasp.ega.ebi.ac.uk:.  . < ~/unagidata/foxp3/wtcc_ega_box78.txt &




nohup ~/programs/ascp/ascp -QTr -l 1000M -k 1 -i asperaweb_id_dsa.putty ega-box-78@fasp.ega.ebi.ac.uk:.  . < ~/programs/ascp/wtcc_ega_box78.txt &




1958BC
AS
ATD
BC
BD
CAD
CD
EGAD00010000230
EGAD00010000232
EGAD00010000234
EGAD00010000236
EGAS00000000026_Cases
EGAS00000000026_Controls
EGAS00000000031
HT
IS
MS
MS_WTCCC1
nohup.out
PBC
PD
PS
RA
T1D
T2D
TB_AFC
UC
WTCCC2_AS
WTCCC2_NBS

for f in `find T1D -type f | grep '.bfe$'`; do echo $f; bcrypt -s0 $f < ~/unagidata/foxp3/wtcc1_code.txt; done


for f in `find -type f | grep '.bfe$'`; do echo $f; bcrypt -s0 $f < ~/unagidata/foxp3/wtcc1_code.txt & done

for f in `find -type f | grep '.bfe$'`; do echo $f; bcrypt -s0 $f < ~/unagidata/foxp3/wtcc58_code.txt; done


for f in `find -type f | grep '.gpg$'`; do newf=`echo $f | sed 's/.gpg$//g'`; echo "$f --> $newf"; gpg --batch --passphrase-file ~/unagidata/foxp3/wtcc58_code.txt --output $newf -d $f & done
for f in `find -type f | grep '.gpg$'`; do newf=`echo $f | sed 's/.gpg$//g'`; echo "$f --> $newf"; gpg --batch --passphrase-file ~/unagidata/foxp3/wtcc2_code.txt --output $newf -d $f & done
for f in `find -type f | grep '.gpg$'`; do newf=`echo $f | sed 's/.gpg$//g'`; echo "$f --> $newf"; gpg --batch --passphrase-file ~/unagidata/foxp3/wtcc1_code.txt --output $newf -d $f & done
for f in `find -type f | grep '.gpg$'`; do newf=`echo $f | sed 's/.gpg$//g'`; echo "$f --> $newf"; gpg --batch --passphrase-file ~/unagidata/foxp3/wtcc3_code.txt --output $newf -d $f & done
for f in `find -type f | grep '.gpg$'`; do newf=`echo $f | sed 's/.gpg$//g'`; echo "$f --> $newf"; gpg --batch --passphrase-file ~/unagidata/foxp3/wtcc_ega.txt  --output $newf -d $f & done
for f in `find -type f | grep '.gpg$'`; do newf=`echo $f | sed 's/.gpg$//g'`; echo "$f --> $newf"; gpg --batch --passphrase-file ~/unagidata/foxp3/wtcc_celiacs.txt  --output $newf -d $f & done
for f in `find ~/foxp3/ega/EGAD00010000246/ -type f | grep '.gpg$'`; do newf=`echo $f | sed 's/.gpg$//g'`; echo "$f --> $newf";  gpg --batch --passphrase-file ~/unagidata/foxp3/wtcc_celiacs.txt  --output $newf -d $f & done
for f in `find ~/foxp3/ega/EGAD00010000250/ -type f | grep '.gpg$'`; do newf=`echo $f | sed 's/.gpg$//g'`; echo "$f --> $newf";  gpg --batch --passphrase-file ~/unagidata/foxp3/wtcc_celiacs.txt  --output $newf -d $f & done
for f in `find | grep 'gpg$'`; do newf=`echo $f | sed 's/.gpg$//g'`; echo "$f --> $newf";  gpg --batch --passphrase-file ~/unagidata/foxp3/wtcc_celiacs.txt  --output $newf -d $f & done


for f in `find ../../ -type f | grep '.gpg$'`; do newf=`echo $f | sed 's/.gpg$//g'`; echo "$f -> $newf"; if [ -f $newf ]; then echo skipping; continue; fi; nohup gpg --batch --passphrase-file ~/unagidata/foxp3/wtcc58_code.txt --output $newf -d $f & done


for f in `find ../../ -type f | grep '.gpg$'`; do newf=`echo $f | sed 's/.gpg$//g'`; echo "$f -> $newf"; if [ -f $newf ]; then echo skipping; continue; fi; gpg --batch --passphrase-file ~/unagidata/foxp3/wtcc2_code.txt --output $newf -d $f & done


