Code base for Arvey et al (2015) eLife
Author: Aaron Arvey
Version: 1.0
Install: Prior to running, aa-utils and many other R packages will need to be installed.
Notes:

  * All raw data has been cached in .rda files that can be obtained from the
    GEO accession associated with the manuscript.  This will make running the code
    much easier than recomputing everything from scratch.  The rda files include
    raw read positions, acetylation/foxp3 peaks and RPM values, human-mouse regulatory
    element mapping and quantification, polymorphism genotypes, and heterozygote 
    allelic read counts. 

  * High level functions are included in main() at top of each elife_* file.  
    It is expected that you'll run these functions in global namespace, 
    whereas all other functions can be called in their local namespace.

  * Code is written to 200 character width.




